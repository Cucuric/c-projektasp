#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

struct CreditCardTransaction {
  int key;
  string cc_num;
  string merchant;
  bool is_fraud;
};

class TransactionFileHandler {
private:
  vector<CreditCardTransaction> transactions;
  string filename;

  void rewriteCsvFile() {
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();

    ofstream file(filename);

    file << "Key,CC_Num,Merchant,Is_Fraud\n";

    for (const auto &trans : transactions) {
      file << trans.key << "," << trans.cc_num << "," << trans.merchant << ","
           << (trans.is_fraud ? "1" : "0") << "\n";
    }
    file.close();
    end = std::chrono::system_clock::now();

    std::chrono::duration<double> elapsed_seconds = end - start;

    std::chrono::duration<double, std::milli> elapsed_milliseconds =
        end - start;

    cout << "Time taken to change CSV: " << elapsed_seconds.count() << " s ["
         << elapsed_milliseconds.count() << " ms]\n\n"
         << endl;
  }

public:
  TransactionFileHandler(const string &filename) : filename(filename) {}

  void readCSV() {
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();

    ifstream file(filename);
    if (!file.is_open()) {
      cout << "Error: File cannot be open: " << filename
           << ". If 'rewriteCVS' is called a new file will be created with "
              "this name"
           << endl;
      return;
    } else {
      cout << "File found and opened successfully" << endl;
    }

    string line;
    getline(file, line);
    if (line.find(",trans_date_trans_time,cc_num,merchant,category,amt,first,"
                  "last,gender,street,city,state,zip,lat,long,city_pop,job,dob,"
                  "trans_num,unix_time,merch_lat,merch_long,is_fraud") !=
        string::npos) {

      while (getline(file, line)) {
        stringstream ss(line);

        string field;
        CreditCardTransaction trans;
        int countComma = 0;

        while (getline(ss, field, ',')) {
          if (countComma == 0)
            trans.key = stoi(field);
          else if (countComma == 2)
            trans.cc_num = field;
          else if (countComma == 3)
            trans.merchant = field;
          else if (countComma == 22 || countComma == 23 || countComma == 24)
            trans.is_fraud = field == "1";
          countComma++;
        }
        transactions.push_back(trans);
      }
    } else {
      while (getline(file, line)) {
        stringstream ss(line);

        string field;
        CreditCardTransaction trans;
        int countComma = 0;

        while (getline(ss, field, ',')) {
          if (countComma == 0)
            trans.key = stoi(field);
          else if (countComma == 1)
            trans.cc_num = field;
          else if (countComma == 2)
            trans.merchant = field;
          else if (countComma == 3)
            trans.is_fraud = field == "1";
          countComma++;
        }
        transactions.push_back(trans);
      }
    }
    file.close();

    end = std::chrono::system_clock::now();

    std::chrono::duration<double> elapsed_seconds = end - start;

    std::chrono::duration<double, std::milli> elapsed_milliseconds =
        end - start;

    cout << "Time taken to readCSV: " << elapsed_seconds.count() << " s ["
         << elapsed_milliseconds.count() << " ms]\n\n"
         << endl;
  }

  void displayAllTransactionInfo() {
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();

    cout << "All Transactions:" << endl;
    for (const auto &trans : transactions) {
      cout << "Key: " << trans.key << ", CC Num: " << trans.cc_num
           << ", Merchant: " << trans.merchant
           << ", Fraud: " << (trans.is_fraud ? "Yes" : "No") << endl;
    }

    end = std::chrono::system_clock::now();

    std::chrono::duration<double> elapsed_seconds = end - start;

    std::chrono::duration<double, std::milli> elapsed_milliseconds =
        end - start;

    cout << "Time taken to display information: " << elapsed_seconds.count()
         << " s [" << elapsed_milliseconds.count() << " ms]\n\n"
         << endl;
  }

  void displayTransactionInfo(const int &key = -1, const string &cc_num = "",
                              const string &merchant = "",
                              const bool &is_fraud = false) {
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();

    bool found = false;

    for (const auto &trans : transactions) {
      if ((key == -1 || trans.key == key) &&
          (cc_num.empty() || trans.cc_num == cc_num) &&
          ((is_fraud && trans.is_fraud) || (!is_fraud && !trans.is_fraud)) &&
          (merchant.empty() || trans.merchant == merchant)) {
        cout << "Key: " << trans.key << ", CC Num: " << trans.cc_num
             << ", Merchant: " << trans.merchant
             << ", Fraud: " << (trans.is_fraud ? "Yes" : "No") << endl;
        found = true;
      }
    }

    if (!found) {
      cout << "No transaction found" << endl;
    }

    end = std::chrono::system_clock::now();

    std::chrono::duration<double> elapsed_seconds = end - start;

    std::chrono::duration<double, std::milli> elapsed_milliseconds =
        end - start;

    cout << "Time taken to display infomation: " << elapsed_seconds.count()
         << " s [" << elapsed_milliseconds.count() << " ms]\n\n"
         << endl;
  }

  void deleteTransaction(const int &key = -1, const string &cc_num = "",
                         const string &merchant = "",
                         const bool &is_fraud = false) {
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();

    bool found = false;
    do {
      auto it = find_if(
          transactions.begin(), transactions.end(),
          [key, cc_num, is_fraud, merchant](const CreditCardTransaction &t) {
            return (key == -1 || t.key == key) &&
                   (cc_num.empty() || t.cc_num == cc_num) &&
                   (is_fraud == false || t.is_fraud == is_fraud) &&
                   (merchant.empty() || t.merchant == merchant);
          });

      if (it != transactions.end()) {
        transactions.erase(it);
        rewriteCsvFile();
        found = true;
      } else {
        found = false;
      }
    } while (found);

    if (found) {
      cout << "All matching transactions deleted successfully." << endl;
    } else {
      cout << "No matching transactions found!" << endl;
    }
    end = std::chrono::system_clock::now();

    std::chrono::duration<double> elapsed_seconds = end - start;

    std::chrono::duration<double, std::milli> elapsed_milliseconds =
        end - start;

    cout << "Time taken to delete: " << elapsed_seconds.count() << " s ["
         << elapsed_milliseconds.count() << " ms]\n\n"
         << endl;
  }

  void addNewTransactions(const string &cc_num = "",
                          const string &merchant = "",
                          const bool &is_fraud = false, int transNum = 1) {
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
    int highestKey = transactions.empty() ? 0 : transactions.back().key;

    for (int i = 0; i < transNum; ++i) {
      CreditCardTransaction newTrans;
      newTrans.key = ++highestKey;
      newTrans.cc_num = cc_num;
      newTrans.merchant = merchant;
      newTrans.is_fraud = is_fraud;
      transactions.push_back(newTrans);
    }

    rewriteCsvFile();
    cout << transNum << " new transaction(s) added successfully." << endl;
    end = std::chrono::system_clock::now();

    std::chrono::duration<double> elapsed_seconds = end - start;

    std::chrono::duration<double, std::milli> elapsed_milliseconds =
        end - start;

    cout << "Time taken to add info: " << elapsed_seconds.count() << " s ["
         << elapsed_milliseconds.count() << " ms]\n\n"
         << endl;
  }

  void displayLowestOrHighestKey(const int &mode, const int &n = 1) {
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
    vector<CreditCardTransaction> sortedTransactions = transactions;

    if (mode == 0){
      cout << "Lowest keys" << endl;
      sort(sortedTransactions.begin(), sortedTransactions.end(),
           [](const CreditCardTransaction &a, const CreditCardTransaction &b) {
             return a.key < b.key;
           });
    }else if (mode == 1){ 
      cout << "Highest keys" << endl;
      sort(sortedTransactions.begin(), sortedTransactions.end(),
           [](const CreditCardTransaction &a, const CreditCardTransaction &b) {
             return a.key > b.key;
           });
    }

    int count = 0;
    for (const auto &trans : sortedTransactions) {
      cout << "Key: " << trans.key << ", CC Num: " << trans.cc_num
           << ", Merchant: " << trans.merchant
           << ", Fraud: " << (trans.is_fraud ? "Yes" : "No") << endl;
      count++;
      if (count == n){
        break;
      }
    }
    end = std::chrono::system_clock::now();

    std::chrono::duration<double> elapsed_seconds = end - start;

    std::chrono::duration<double, std::milli> elapsed_milliseconds =
        end - start;

    cout << "Time taken to display highest/lowest key: "
         << elapsed_seconds.count() << " s [" << elapsed_milliseconds.count()
         << " ms]\n\n"
         << endl;
  }

  void displayLowestOrHighestCCNum(const int &mode, const int &n = 1) {
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
    vector<CreditCardTransaction> sortedTransactions = transactions;

    if (mode == 0){
      cout << "Lowest cc_num" << endl;
      sort(sortedTransactions.begin(), sortedTransactions.end(),
           [](const CreditCardTransaction &a, const CreditCardTransaction &b) {
             return a.cc_num < b.cc_num;
           });
    } else if (mode == 1){
      cout << "Highest cc_num" << endl;
      sort(sortedTransactions.begin(), sortedTransactions.end(),
           [](const CreditCardTransaction &a, const CreditCardTransaction &b) {
             return a.cc_num > b.cc_num;
           });
    }

    int count = 0;
    for (const auto &trans : sortedTransactions) {
      cout << "Key: " << trans.key << ", CC Num: " << trans.cc_num
           << ", Merchant: " << trans.merchant
           << ", Fraud: " << (trans.is_fraud ? "Yes" : "No") << endl;
      count++;
      if (count == n){        
        break;
      }
    }
    end = std::chrono::system_clock::now();

    std::chrono::duration<double> elapsed_seconds = end - start;

    std::chrono::duration<double, std::milli> elapsed_milliseconds =
        end - start;

    cout << "Time taken to display highest/lowest cc_num: "
         << elapsed_seconds.count() << " s [" << elapsed_milliseconds.count()
         << " ms]\n\n"
         << endl;
  }

  void displayMerchantsAlphabetically(const int &mode, const int &n = 1) {
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
    vector<CreditCardTransaction> sortedTransactions = transactions;

    if (mode == 0){
      cout << "Merchants with 'higher' alphabetical value" << endl;
      sort(sortedTransactions.begin(), sortedTransactions.end(),
           [](const CreditCardTransaction &a, const CreditCardTransaction &b) {
             return a.merchant < b.merchant;
           });
    }else if (mode == 1){
       cout << "Merchants with 'lower' alphabetical value" << endl;
      sort(sortedTransactions.begin(), sortedTransactions.end(),
           [](const CreditCardTransaction &a, const CreditCardTransaction &b) {
             return a.merchant > b.merchant;
           });
    }

    int count = 0;
    for (const auto &trans : sortedTransactions) {
      cout << "Key: " << trans.key << ", CC Num: " << trans.cc_num
           << ", Merchant: " << trans.merchant
           << ", Fraud: " << (trans.is_fraud ? "Yes" : "No") << endl;
      count++;
      if (count == n){
        break;
      }
    }
    end = std::chrono::system_clock::now();

    std::chrono::duration<double> elapsed_seconds = end - start;

    std::chrono::duration<double, std::milli> elapsed_milliseconds =
        end - start;

    cout << "Time taken to display merchant by alphabetical order: "
         << elapsed_seconds.count() << " s [" << elapsed_milliseconds.count()
         << " ms]\n\n"
         << endl;
  }

  void displayBoolFraud(const int &mode, const int &n = 1) {
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
    vector<CreditCardTransaction> sortedTransactions = transactions;

    if (mode == 0){
       cout << "No fraud: " << endl;
      sort(sortedTransactions.begin(), sortedTransactions.end(),
           [](const CreditCardTransaction &a, const CreditCardTransaction &b) {
             if (a.is_fraud == b.is_fraud)
               return a.key < b.key;
             return a.is_fraud < b.is_fraud;
           });
    }else if (mode == 1){
       cout << "Fraud: " << endl;
      sort(sortedTransactions.begin(), sortedTransactions.end(),
           [](const CreditCardTransaction &a, const CreditCardTransaction &b) {
             if (a.is_fraud == b.is_fraud)
               return a.key < b.key;
             return a.is_fraud > b.is_fraud;
           });
    }

    int count = 0;
    for (const auto &trans : sortedTransactions) {
      if ((mode == 0 && !trans.is_fraud) || (mode == 1 && trans.is_fraud)) {
        cout << "Key: " << trans.key << ", CC Num: " << trans.cc_num
             << ", Merchant: " << trans.merchant
             << ", Fraud: " << (trans.is_fraud ? "Yes" : "No") << endl;
        count++;
      }
      if (count == n){
        break;
      }
    }
    end = std::chrono::system_clock::now();

    std::chrono::duration<double> elapsed_seconds = end - start;

    std::chrono::duration<double, std::milli> elapsed_milliseconds =
        end - start;

    cout << "Time taken to display true/false frauds: "
         << elapsed_seconds.count() << " s [" << elapsed_milliseconds.count()
         << " ms]\n\n" << endl;
  }

  void delCCnum(const int &key) {
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();

    cout << "Deleting cc_num: " << key;
    bool found = false;
    for (auto &trans : transactions) {
      if (trans.key == key) {
        trans.cc_num = "";
        rewriteCsvFile();
        found = true;
        break;
      }
    }

    if (found) {
      cout << "Transaction with key " << key << " updated successfully."
           << endl;
    } else {
      cout << "No matching transaction found!" << endl;
    }

    end = std::chrono::system_clock::now();

    std::chrono::duration<double> elapsed_seconds = end - start;
    std::chrono::duration<double, std::milli> elapsed_milliseconds =
        end - start;

    cout << "Time taken to delete: " << elapsed_seconds.count() << " s ["
         << elapsed_milliseconds.count() << " ms]\n\n"
         << endl;
  }

  void delMerchant(const int &key) {
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();

    cout << "Deleting merchant: " << key;
    bool found = false;
    for (auto &trans : transactions) {
      if (trans.key == key) {
        trans.merchant = "";
        rewriteCsvFile();
        found = true;
        break;
      }
    }

    if (found) {
      cout << "Merchant for transaction with key " << key
           << " deleted successfully." << endl;
    } else {
      cout << "No matching transaction found!" << endl;
    }

    end = std::chrono::system_clock::now();

    std::chrono::duration<double> elapsed_seconds = end - start;
    std::chrono::duration<double, std::milli> elapsed_milliseconds =
        end - start;

    cout << "Time taken to delete merchant: " << elapsed_seconds.count()
         << " s [" << elapsed_milliseconds.count() << " ms]\n\n"
         << endl;
  }

  void changeFraud(const int &key) {
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();

    cout << "Chaning if_fraud bool: " << key;
    bool found = false;
    for (auto &trans : transactions) {
      if (trans.key == key) {
        trans.is_fraud = !trans.is_fraud;
        rewriteCsvFile();
        found = true;
        break;
      }
    }

    if (found) {
      cout << "Is_fraud for transaction with key " << key
           << " toggled successfully." << endl;
    } else {
      cout << "No matching transaction found!" << endl;
    }

    end = std::chrono::system_clock::now();

    std::chrono::duration<double> elapsed_seconds = end - start;
    std::chrono::duration<double, std::milli> elapsed_milliseconds =
        end - start;

    cout << "Time taken to toggle is_fraud: " << elapsed_seconds.count()
         << " s [" << elapsed_milliseconds.count() << " ms]\n\n"
         << endl;
  }

  void addCCNum(const int &key, const string &newCCNum) {
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();

    cout << "Adding cc_nmun: " << key;
    bool found = false;
    for (auto &trans : transactions) {
      if (trans.key == key && trans.cc_num.empty()) {
        trans.cc_num = newCCNum;
        rewriteCsvFile();
        found = true;
        break;
      }
    }

    if (found) {
      cout << "CC_num added successfully to transaction with key " << key << "."
           << endl;
    } else {
      cout << "No matching transaction found or CC_num slot is not empty!"
           << endl;
    }

    end = std::chrono::system_clock::now();

    std::chrono::duration<double> elapsed_seconds = end - start;
    std::chrono::duration<double, std::milli> elapsed_milliseconds =
        end - start;

    cout << "Time taken to add CC_num: " << elapsed_seconds.count() << " s ["
         << elapsed_milliseconds.count() << " ms]\n\n"
         << endl;
  }

  void addMerchant(const int &key, const string &newMerchant) {
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();

    cout << "Adding merchant: " << key;
    bool found = false;
    for (auto &trans : transactions) {
      if (trans.key == key && trans.merchant.empty()) {
        trans.merchant = newMerchant;
        rewriteCsvFile();
        found = true;
        break;
      }
    }

    if (found) {
      cout << "Merchant added successfully to transaction with key " << key
           << "." << endl;
    } else {
      cout << "No matching transaction found or Merchant slot is not empty!"
           << endl;
    }

    end = std::chrono::system_clock::now();

    std::chrono::duration<double> elapsed_seconds = end - start;
    std::chrono::duration<double, std::milli> elapsed_milliseconds =
        end - start;

    cout << "Time taken to add Merchant: " << elapsed_seconds.count() << " s ["
         << elapsed_milliseconds.count() << " ms]\n\n"
         << endl;
  }
};

int main() {
  TransactionFileHandler reader("data.csv");
  reader.readCSV();

  reader.displayAllTransactionInfo();


  //Dohvaćanje najveće/najmanje vrijednosti

  /*reader.displayLowestOrHighestKey(0,5);

  reader.displayLowestOrHighestKey(1,5);

  reader.displayLowestOrHighestCCNum(0,5);

  reader.displayLowestOrHighestCCNum(1,5);

  reader.displayMerchantsAlphabetically(0,5);

  reader.displayMerchantsAlphabetically(1,5);

  reader.displayBoolFraud(0,5);

  reader.displayBoolFraud(1,5);*/

  //Dohvaćanje vrijednosti prema parametrima

  /*reader.displayTransactionInfo(4);
  
  reader.displayTransactionInfo(-1,"3573030041201292");
  
  reader.displayTransactionInfo(-1,"","fraud_Haley Group");
  
  reader.displayTransactionInfo(-1,"","",false);*/

  //Dodavanje novih vrijednosti

  /*reader.addNewTransactions();

  reader.addNewTransactions("123");

  reader.addNewTransactions("","Testing");

  reader.addNewTransactions("","",true);

  reader.addNewTransactions("","",false,3);

  reader.addNewTransactions("123123","Hello",true);*/

  //Brisanje vrijednosti

  /*reader.deleteTransaction(10);

  reader.deleteTransaction(-1,"123");

  reader.deleteTransaction(-1,"","Testing");
  
  reader.deleteTransaction(-1,"123123","Hello",true);*/
  
    
  
  return 0;
}
